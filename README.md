# OpenML dataset: NSE-Future-and-Options-Dataset-3M

https://www.openml.org/d/43848

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This Data is gathered from NSE website  for the past three months I am posting this here so people can analyse this data 
 and gather meaningful insights from this.
Example -  Probability of Stock ending up at Max Pain with the help of Open Interest.
Content
The dataset contains stock symbol with which it is traded,  Expiry Date. Strike Price and the Option pricing of the Symbol at that Strike price.
Acknowledgements
I thank the people working at NSE for publishing these reports everyday.
Inspiration
Whenever we want to initiate an Options trade we look at various parameters like OpenInterest, Change in OI, Technical Analysis Indicators before deciding to Buy/Sell the Option. Most times we need to browse to multiple websites to gather the data we need, This is an example to show how you can customise the data for our needs.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43848) of an [OpenML dataset](https://www.openml.org/d/43848). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43848/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43848/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43848/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

